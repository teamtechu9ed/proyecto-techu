require('dotenv').config();

const crypt = require('../crypt');
const promise = require('promise');
var nodemailer = require('nodemailer');
const requestJSON = require('request-json');
const userController = require('./UserController');
const { check, validationResult } = require('express-validator/check');

const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;
const baseMlabURL = "https://api.mlab.com/api/1/databases/techu-bpjjsa/collections/";

/**********/
/* GOOGLE */
/**********/

const { google } = require('googleapis');
const plus = google.plus('v1');
const oauth2 = google.oauth2('v2');
const gmail = google.gmail('v1');

var oauth2Client = null;
var keys = JSON.parse(process.env.GOOGLE_CREDENTIALS.replace(/URL_PROYECTO_FRONT_TECHU/g, process.env.URL_PROYECTO_FRONT_TECHU));

function postLoginV1(req, res) {
    console.log("POST /techu-bpjjsa/v1/login");

    var errors = validationResult(req);

    if (!errors.isEmpty()) {
        res.status(400).json(errors.array());
    } else {
        var body = req.body;

        var user = {};
        user.email = body.email;
        user.password = body.password;

        var httpClient = requestJSON.createClient(baseMlabURL);
        console.log("Cliente creado");

        var resultado = {}
        var query = "q={\"email\":\"" + user.email + "\"}";
        console.log("Query: " + baseMlabURL + "user?" + query);

        // Buscamos al usuario en base a su email
        httpClient.get("user?" + query + "&" + mLabAPIKey,
            function (err, resMlab, body) {
                if (err) {
                    resultado = [{
                        "location": "body",
                        "msg": "Email y/o contraseña incorrecta"
                    }]
                    res.status(500);
                } else {
                    if (body.length > 0) {
                        var valido = crypt.checkPassword(user.password, body[0].password);
                        if (valido === true) {
                            resultado = {
                                "id_user": body[0].id_user,
                                "display_name": body[0].display_name,
                                "last_name": body[0].last_name,
                                "email": body[0].email,
                                "last_acces_date": body[0].last_acces_date,
                                "picture": body[0].picture,
                            }

                            // No actualizamos la fecha de acceso al entrar
                            // sino al mostrar el dashboard
                            req.session.user = JSON.stringify(body[0]);
                            console.log("req.session.user");
                            console.log(req.session.user);

                            res.status(200);
                        } else {
                            resultado = [{
                                "location": "body",
                                "msg": "Email y/o contraseña incorrecta"
                            }]
                            res.status(401);
                        }
                    } else {
                        resultado = [{
                            "location": "body",
                            "msg": "Email y/o contraseña incorrecta"
                        }]
                        res.status(401);
                    }
                }

                res.send(resultado);
            }
        );
    }
}

function postLogoutV1(req, res) {
    console.log("POST /techu-bpjjsa/v1/logout");

    var loggedUser = req.session.user;
    console.log("loggedUser");
    console.log(loggedUser);

    console.log("req.cookies.user_sid");
    console.log(req.cookies.user_sid);

    var resultado = [];

    if (loggedUser && req.cookies.user_sid) {
        var JSONLoggedUser = JSON.parse(loggedUser);
        console.log("JSONLoggedUser.id_user");
        console.log(JSONLoggedUser.id_user);

        var resultado = [{
            "msg": "logout correcto",
            "idUsuario": JSONLoggedUser.id_user
        }]

        req.session.user = null;
        res.status(200);
    } else {
        var resultado = [{
            "msg": "logout incorrecto",
        }]
        res.status(401);
    }
    res.clearCookie('user_sid');
    res.send(resultado);
}

function postResetPasswordUserV1(req, res) {
    console.log("POST /techu-bpjjsa/v1/resetPassword");

    var errors = validationResult(req);

    if (!errors.isEmpty()) {
        res.status(400).json(errors.array());
    } else {
        var body = req.body;

        var user = {};
        user.email = body.email;
        console.log("user: " + JSON.stringify(user));

        var httpClient = requestJSON.createClient(baseMlabURL);
        console.log("Cliente creado");

        var resultado = {}
        var query = "q={\"email\":\"" + user.email + "\"}";
        console.log("Query: " + baseMlabURL + "user?" + query);

        // Buscamos al usuario en base a su email
        httpClient.get("user?" + query + "&" + mLabAPIKey,
            function (err, resMlab, body) {
                if (err) {
                    res.status(500);
                } else {
                    if (body.length > 0) {
                        // el usuario existe

                        var timestamp = (new Date()).getTime();
                        var token = crypt.hash(user.email + timestamp);

                        // actualizamos el token utilizado y la hora en la que se ha generado
                        var putBody = '{"$set":{' +
                            '"reset_password": {' +
                            '"token": "' + token + '",' +
                            '"reset_date": ' + timestamp +
                            '}' +
                            '}' +
                            '}';

                        console.log("putBody");
                        console.log(putBody);

                        httpClient.put("user?" + query + "&" + mLabAPIKey,
                            JSON.parse(putBody),
                            function (err, resMlab, body) {
                                if (err) {
                                    // el usuario no debe saber que ha ido mal
                                    res.status(200);
                                } else {
                                    // se procede a mandar un correo con un enlace (token)
                                    var transporter = nodemailer.createTransport({
                                        service: 'Gmail',
                                        auth: {
                                            user: process.env.USER_SEND_EMAIL,
                                            pass: process.env.PASSWORD_SEND_EMAIL
                                        }
                                        /*
                                        ,tls: {
                                            rejectUnauthorized: false
                                        }
                                        */
                                    });

                                    const mailOptions = {
                                        from: '"Money Money (noresponder@moneymoney.com)" <noresponder@moneymoney.com>', // sender address
                                        to: user.email, // list of receivers
                                        subject: 'Resetear password Money Money', // Subject line
                                        html: "Saludos desde Money Money,<br><br>" +
                                            "Hemos recibido recientemente una petición de <span class=\"il\">reseteo</span> de contraseña para su cuenta \"" + user.email + "\".&nbsp; &nbsp;" +
                                            "Con el fin de llevar a cabo la petición, pulse sobre el siguiente link:<br><br>&nbsp; &nbsp;" +
                                            "<a href=\"" + process.env.URL_PROYECTO_FRONT_TECHU + "/reset/user?token=" + token + "\" rel=\"noreferrer\" target=\"_blank\">" + process.env.URL_PROYECTO_FRONT_TECHU + "/reset/user?token=" + token + "</a><br>" +
                                            "<br>Para su protección, el enlace solamente es de un sólo uso, y únicamente es válido durante (30) minutos.<br>" +
                                            "<br>Sí usted no ha solicitado el <span class=\"il\">reseteeo</span> de su password, lo más probable que haya ocurrido es que alguien ingresó por error su nombre de usuario de Money Money.<br>" +
                                            "<br>Si tiene alguna duda o pregunta, comuníquese por favor con nosotros en cualquier momento en la siguiente dirección <a href=\"mailto:soporte@moneymoney.com\" target=\"_blank\">soporte@moneymoney.com</a>.<br>" +
                                            "<br><br>Atentamente, <br>Money Money<br><br>"
                                    };

                                    transporter.sendMail(mailOptions, function (err, info) {
                                        if (err) {
                                            console.log(err)
                                            // el usuario no debe saber que ha ido mal
                                        } else {
                                            console.log("Email enviado satisfactoriamente");
                                            console.log(info);
                                        }
                                    });

                                    // se almacena tanto el token como la fecha de envio (TTL)
                                    res.status(200);
                                }
                            }
                        )
                    } else {
                        // el usuario no debe saber que ha ido mal
                        res.status(200);
                    }
                }

                resultado = [{
                    "location": "body",
                    "msg": "Revisa la bandeja de entrada y la de spam, de: " + user.email + "."
                }, {
                    "location": "body",
                    "msg": "Si no te llega el correo, puede que te registraras con Google y no te acuerdes."
                }, {
                    "location": "body",
                    "msg": "Intenta acceder de nuevo con tu cuenta de Google."
                }]

                res.send(resultado);
            }
        );
    }
}

function patchResetPasswordUserV1(req, res) {
    console.log("PATCH /techu-bpjjsa/v1/resetPassword");

    var errors = validationResult(req);

    if (!errors.isEmpty()) {
        res.status(400).json(errors.array());
    } else {
        var body = req.body;

        var user = {};
        user.email = body.email;
        user.password = body.password;
        user.reset_password = {
            token: body.token
        }

        console.log("user: " + JSON.stringify(user));

        var httpClient = requestJSON.createClient(baseMlabURL);
        console.log("Cliente creado");

        var resultado = {}
        var query = "q={\"email\":\"" + user.email + "\"}";
        console.log("Query: " + baseMlabURL + "user?" + query);

        // Buscamos al usuario en base a su email
        httpClient.get("user?" + query + "&" + mLabAPIKey,
            function (err, resMlab, body) {
                if (err) {
                    resultado = [{
                        "location": "body",
                        "msg": "Error al procesar la petición"
                    }]

                    res.status(500);
                } else {
                    if (body.length > 0) {
                        // el usuario existe
                        console.log("Existe un usuario con ese email");
                        if ((body[0].reset_password != null) && (typeof (body[0].reset_password) != "undefined")) {
                            // el usuario ha solicitado un reseteo de pwd
                            console.log("El usuario solicitó un reseteo de la pwd");
                            if (body[0].reset_password.token == user.reset_password.token) {
                                // comprobamos el timestamp
                                console.log("El token es válido");
                                var timestampActual = (new Date()).getTime();
                                console.log("timestampActual: " + timestampActual);

                                var timestampReset = body[0].reset_password.reset_date;
                                console.log("timestampReset: " + timestampReset);

                                const maxtime = 30 * 60 * 1000;
                                var time = timestampActual - timestampReset;

                                if (time <= maxtime) {
                                    console.log("Se encuentra dentro de los 30 minutos de plazo");
                                    // debemos actualizar la pwd y eliminar el token ya usado

                                    // actualizamos el token utilizado y la hora en la que se ha generado
                                    var putBody = '{' +
                                        '"$unset":{' +
                                        '"reset_password": ""' +
                                        '},' +
                                        '"$set":{' +
                                        '"password": "' + crypt.hash(req.body.password) + '"' +
                                        '}' +
                                        '}';

                                    console.log("putBody");
                                    console.log(putBody);

                                    httpClient.put("user?" + query + "&" + mLabAPIKey,
                                        JSON.parse(putBody),
                                        function (err, resMlab, body) {
                                            if (err) {
                                                console.log("Error al actualizar los datos en la BBDD");
                                                console.log(err);

                                                resultado = [{
                                                    "location": "body",
                                                    "msg": "Error al procesar la petición"
                                                }]

                                                res.status(401);
                                            } else {
                                                resultado = [{
                                                    "location": "body",
                                                    "msg": "Usuario modificado con éxito"
                                                }]
                                                res.status(200);
                                            }
                                        }
                                    )

                                } else {
                                    console.log("No se encuentra dentro de los 30 minutos de plazo");
                                    resultado = [{
                                        "location": "body",
                                        "msg": "Error al procesar la petición"
                                    }]

                                    res.status(401);
                                }
                            } else {
                                console.log("El token no es válido");
                                resultado = [{
                                    "location": "body",
                                    "msg": "Error al procesar la petición"
                                }]

                                res.status(401);
                            }
                        } else {
                            console.log("El usuario no solicitó un reseteo de la pwd");
                            resultado = [{
                                "location": "body",
                                "msg": "Error al procesar la petición"
                            }]

                            res.status(401);
                        }
                    } else {
                        console.log("No existe un usuario con ese email");
                        resultado = [{
                            "location": "body",
                            "msg": "Error al procesar la petición"
                        }]

                        res.status(401);
                    }
                }

                res.send(resultado);
            }
        );
    }
}

function getOAuthURL(req, res) {
    console.log("GET /techu-bpjjsa/v1/auth/google/authorize");

    var redirectUri = keys.redirect_uris[0];

    if(req.headers.referer != null && req.headers.referer.indexOf("create") != -1){
        redirectUri = keys.redirect_uris[1];
    }

    oauth2Client = new google.auth.OAuth2(
        keys.client_id,
        keys.client_secret,
        redirectUri
    );
    
    /**
     * This is one of the many ways you can configure googleapis to use authentication credentials.  In this method, we're setting a global reference for all APIs.  Any other API you use here, like google.drive('v3'), will now use this auth client. You can also override the auth client at the service and method call levels.
     */
    google.options({ auth: oauth2Client });

    const scopes = ['https://www.googleapis.com/auth/plus.me',
        'https://www.googleapis.com/auth/userinfo.profile',
        'https://www.googleapis.com/auth/userinfo.email'];

    const authorizeUrl = oauth2Client.generateAuthUrl({
        access_type: 'offline',
        scope: scopes.join(' '),
    });

    resultado = {
        "msg": authorizeUrl
    }
    res.status(200);
    res.send(resultado);
}

function getOAuthCallback(req, res) {
    console.log("GET /techu-bpjjsa/v1/auth/google/callback");

    var resultado = {};

    // recogemos el authorization code
    var code = req.query.code;
    oauth2Client.getToken(code,
        function (err, bodyTokens) {
            if (err) {
                console.log("Error en getToken");
                console.log(err);
                resultado = [{
                    "location": "body",
                    "msg": "Error al obtener sesión de Google"
                }]
                res.status(401);
                res.send(resultado);
            } else {
                // Debemos guardar la credenciales en BBDD
                console.log("bodyTokens");
                console.log(bodyTokens);

                oauth2Client.credentials = bodyTokens;

                // consultamos los scopes del usuario
                console.log("Consulta el perfil de Google");
                oauth2.userinfo.v2.me.get({},
                    function (err, bodyScope) {
                        if (err) {
                            console.log("Error en get.plus.people");
                            console.log(err);
                            resultado = [{
                                "location": "body",
                                "msg": "Error al recuperar la información del perfil de Google"
                            }]
                            res.status(401);
                            res.send(resultado);
                        } else {
                            console.log("bodyScope.data");
                            console.log(bodyScope.data);

                            var httpClient = requestJSON.createClient(baseMlabURL);

                            // debemos revisar si el cliente existe ya en el sistema
                            console.log("Comprobamos si existe el email: " + bodyScope.data.email);

                            // comprobamos que no exista un mismo email ya creado
                            var queryEmail = "q={\"email\":\"" + bodyScope.data.email + "\"}";
                            console.log("Cliente creado: " + baseMlabURL + "user?" + queryEmail);

                            httpClient.get("user?" + queryEmail + "&" + mLabAPIKey,
                                function (err, resMlab, bodyResponse) {
                                    if (err) {
                                        console.log("err: ");
                                        console.log(err);

                                        resultado = [{
                                            "location": "body",
                                            "msg": "Error al obtener los datos del usuario"
                                        }]
                                        res.status(500);
                                        res.send(resultado);
                                    } else {
                                        if (bodyResponse.length > 0) {
                                            // si existe debemos actualizar los datos de perfil y sesión y dar sesión

                                            // actualizamos en BBDD
                                            var putBody = '{' +
                                                '"$set": {' +
                                                '"google": ' + JSON.stringify(bodyScope.data) + ',' +
                                                '"google_credentials": ' + JSON.stringify(bodyTokens) + ',' +
                                                '"last_acces_date": ' +  (new Date()).getTime() +
                                                '}' +
                                                '}';

                                            console.log("putBody");
                                            console.log(JSON.parse(putBody));

                                            httpClient.put("user?" + queryEmail + "&" + mLabAPIKey,
                                                JSON.parse(putBody),
                                                function (err, resMlab, body) {
                                                    if (err) {
                                                        resultado = [{
                                                            "location": "body",
                                                            "msg": "Error al actualizar los datos"
                                                        }]
                                                        res.status(401);
                                                        res.send(resultado);
                                                    } else {
                                                        console.log("Usuario modificado con éxito");
                                                        console.log(body);

                                                        if (typeof (bodyScope.data.bbva) != 'undefined' && typeof (bodyScope.data.bbva.user) != 'undefined') {
                                                            resultado = {
                                                                "id_user": bodyResponse[0].id_user,
                                                                "display_name": bodyResponse[0].display_name,
                                                                "last_name": bodyResponse[0].last_name,
                                                                "email": bodyScope.data.email,
                                                                "last_acces_date": bodyScope.data.last_acces_date,
                                                                "bbva": {
                                                                    "user": bodyScope.data.bbva.user
                                                                }
                                                            }
                                                        }else{
                                                            resultado = {
                                                                "id_user": bodyResponse[0].id_user,
                                                                "display_name": bodyResponse[0].display_name,
                                                                "last_name": bodyResponse[0].last_name,
                                                                "email": bodyScope.data.email,
                                                                "last_acces_date": bodyScope.data.last_acces_date
                                                            }
                                                        }

                                                        // tenemos que dar sesion                                
                                                        req.session.user = JSON.stringify(bodyResponse[0]);
                                                        console.log("req.session.user");
                                                        console.log(req.session.user);
                                                        res.status(200);
                                                        res.send(resultado);
                                                    }
                                                }
                                            )
                                        } else {
                                            console.log("El email no existe en la plataforma, lo creamos");
                                            // si no existe debemos crear el usuario
                                            // obtenemos el ID
                                            var queryID = "s={\"id_user\": -1}&l=-1";
                                            console.log("Cliente creado: " + baseMlabURL + "user?" + queryID);

                                            httpClient.get("user?" + queryID + "&" + mLabAPIKey,
                                                function (err, resMlab, body) {
                                                    if (!err) {
                                                        if (body.length > 0) {
                                                            usersLen = body[0].id_user + 1;
                                                            console.log("El primer identificador libre es: " + usersLen);
                                                        }
                                                    }

                                                    var newUser = {};
                                                    newUser.id_user = usersLen;
                                                    newUser.display_name = bodyScope.data.given_name;
                                                    newUser.last_name = bodyScope.data.family_name;
                                                    newUser.email = bodyScope.data.email;
                                                    newUser.last_acces_date = null;
                                                    newUser.register_date = new Date().getTime();
                                                    newUser.google = bodyScope.data;
                                                    newUser.google_credentials = bodyTokens;

                                                    //console.log("user: " + JSON.stringify(newUser));

                                                    httpClient.post("user/?" + mLabAPIKey, newUser,
                                                        function (err, resMLab, body) {
                                                            console.log("Usuario guardado con éxito");
                                                            console.log(body);

                                                            resultado = {
                                                                "id_user": newUser.id_user,
                                                                "display_name": newUser.display_name,
                                                                "last_name": newUser.last_name,
                                                                "email": newUser.email,
                                                                "last_acces_date": null
                                                            }

                                                            // tenemos que dar sesion                                
                                                            req.session.user = JSON.stringify(newUser);
                                                            console.log("req.session.user");
                                                            console.log(req.session.user);

                                                            res.status(201);
                                                            res.send(resultado);
                                                        }
                                                    )
                                                }
                                            );
                                        }
                                    }
                                }
                            );
                        }
                    }
                );
            }
        }
    );
}

module.exports.getOAuthURL = getOAuthURL;
module.exports.getOAuthCallback = getOAuthCallback;

module.exports.postLoginV1 = postLoginV1;
module.exports.postLogoutV1 = postLogoutV1;
module.exports.postResetPasswordUserV1 = postResetPasswordUserV1;
module.exports.patchResetPasswordUserV1 = patchResetPasswordUserV1;