require('dotenv').config();

const crypt = require('../crypt');
const promise = require('promise');
const requestJSON = require('request-json');
const { check, validationResult } = require('express-validator/check');

const baseBBVA = "https://www.bbva.es/ASO/";
const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;
const baseMlabURL = "https://api.mlab.com/api/1/databases/techu-bpjjsa/collections/";

function getAccountsByIdV1(req, res) {
    console.log("GET /techu-bpjjsa/v1/accounts");

    var errors = validationResult(req);

    if (!errors.isEmpty()) {
        res.status(400).json(errors.array());
    } else {
        console.log("id_user es " + req.query.id_user);

        var loggedUser = req.session.user;
        console.log("loggedUser");
        console.log(loggedUser);

        console.log("req.cookies.user_sid");
        console.log(req.cookies.user_sid);

        if (loggedUser && req.cookies.user_sid) {
            var JSONLoggedUser = JSON.parse(loggedUser);
            console.log("JSONLoggedUser.id_user");
            console.log(JSONLoggedUser.id_user);

            if (JSONLoggedUser.id_user == req.query.id_user) {
                console.log("Identificador de usuario correcto")
                console.log("req.query.refresh: " + req.query.refresh);

                var httpClient = requestJSON.createClient(baseMlabURL);

                // Recuperamos si el usuario tiene o no credenciales en otros bancos
                var query = "q={\"id_user\": " + req.query.id_user + "}&f={\"_id\": 0, \"password\": 0}";
                console.log("Cliente creado: " + baseMlabURL + "user?" + query);

                httpClient.get("user?" + query + "&" + mLabAPIKey,
                    // resMlab - contiene toda la información de la API
                    function (err, resMlab, body) {
                        var response = {};
                        if (err) {
                            response = {
                                "msg": "Error obteniendo usuarios"
                            }
                            res.status(500);
                            res.send(response);
                        } else {
                            if (req.query.refresh != null && req.query.refresh == "true") {
                                // llamamos al GT
                                // recogemos las credenciales de banco
                                if ((body[0].bbva != null) && (typeof (body[0].bbva) != "undefined")) {
                                    bbva_user = body[0].bbva.user;
                                    bbva_password = crypt.decipherText(body[0].bbva.password);

                                    var httpClientBBVA = requestJSON.createClient(baseBBVA);
                                    console.log("Cliente creado: " + baseBBVA + "TechArchitecture/grantingTickets/V02");

                                    var postBody = '{' +
                                        '"authentication":{' +
                                        '"consumerID":"00000130",' +
                                        '"authenticationData":[{' +
                                        '"authenticationData":["' + bbva_password + '"],' +
                                        '"idAuthenticationData":"password"' +
                                        '}],' +
                                        '"authenticationType":"02",' +
                                        '"userID":"0019-0' + bbva_user.toUpperCase() + '"' + // tener en cuenta formato, rellenar con ceros
                                        '}' +
                                        '}';

                                    httpClientBBVA.post("TechArchitecture/grantingTickets/V02",
                                        JSON.parse(postBody),
                                        function (err, resBBVAGT, bodyGT) {
                                            if (err) {
                                                console.log("Error en la llamada al GT");
                                                console.log(err);
                                                response = {
                                                    "msg": "Error durante el proceso de autenticación en BBVA"
                                                }
                                                res.status(500);
                                                res.send(response);
                                            } else {
                                                if ((resBBVAGT.headers.tsec != null) && (typeof (resBBVAGT.headers.tsec) != "undefined")) {
                                                    console.log("bodyGT.user.id: " + bodyGT.user.id);
                                                    console.log("bodyGT.user.alias: " + bodyGT.user.alias);
                                                    console.log("bodyGT.user.person.id: " + bodyGT.user.person.id);

                                                    console.log("resBBVAGT.headers.tsec");
                                                    console.log(resBBVAGT.headers.tsec);

                                                    httpClientBBVA.headers['tsec'] = resBBVAGT.headers.tsec;
                                                    httpClientBBVA.get("psd2/v0/accounts?customerId=" + bodyGT.user.id,
                                                        function (err, resBBVAAccounts, bodyAccounts) {
                                                            if (err) {
                                                                console.log("Error al obtener las cuentas del usuario");
                                                                console.log(err);
                                                                response = {
                                                                    "msg": "Error al obtener las cuentas en BBVA"
                                                                }

                                                                deleteTSEC(resBBVAGT.headers.tsec);

                                                                res.status(500);
                                                                res.send(response);
                                                            } else {
                                                                // actualizamos la información en BBDD
                                                                // debemos revisar si existe el registro
                                                                var queryExistAccountUser = "q={\"id_user\": " + req.query.id_user + "}&f={\"_id\": 0}";
                                                                console.log("Cliente creado: " + baseMlabURL + "account?" + queryExistAccountUser);

                                                                httpClient.get("account?" + queryExistAccountUser + "&" + mLabAPIKey,
                                                                    // resMlab - contiene toda la información de la API
                                                                    function (err, resMlab, bodyExistAccountUser) {
                                                                        if (err) {
                                                                            console.log("Error al obtener si el usuario tiene cuentas asociadas");
                                                                            console.log(err);
                                                                            response = {
                                                                                "msg": "Error obteniendo usuarios"
                                                                            }

                                                                            deleteTSEC(resBBVAGT.headers.tsec);

                                                                            res.status(500);
                                                                            res.send(response);
                                                                        } else {
                                                                            if (bodyExistAccountUser.length > 0) {
                                                                                // si existe actualizarlo
                                                                                var putBody = '{"$set":{' +
                                                                                    '"last_acces_update": ' + new Date().getTime() + ',' +
                                                                                    '"items": ' + JSON.stringify(bodyAccounts.items) +
                                                                                    '}' +
                                                                                    '}';

                                                                                console.log("putBody");
                                                                                console.log(putBody);

                                                                                httpClient.put("account?" + queryExistAccountUser + "&" + mLabAPIKey,
                                                                                    JSON.parse(putBody),
                                                                                    function (err, resMlab, body) {
                                                                                        if (err) {
                                                                                            console.log("Error al guardar en Mlab los datos");
                                                                                            console.log(err);
                                                                                            response = [{
                                                                                                "location": "body",
                                                                                                "msg": "Error al actualizar los datos"
                                                                                            }]

                                                                                            deleteTSEC(resBBVAGT.headers.tsec);

                                                                                            res.status(401);
                                                                                            res.send(response);
                                                                                        } else {
                                                                                            console.log("Cuentas actualizadas correctamente");

                                                                                            // al acctualizar las cuentas, debemos actualizar los movimientos
                                                                                            if (bodyAccounts.items != null && bodyAccounts.items.length > 0) {
                                                                                                bodyAccounts.items.forEach(function (element) {
                                                                                                    updateTransactionsAccountById(req.query.id_user, element.id);
                                                                                                });
                                                                                            }

                                                                                            deleteTSEC(resBBVAGT.headers.tsec);

                                                                                            res.status(200);

                                                                                            var userAccounts = Object.assign({}, {
                                                                                                "id_user": req.query.id_user,
                                                                                                "last_acces_update": new Date().getTime()
                                                                                            },
                                                                                                bodyAccounts);

                                                                                            res.send(userAccounts);
                                                                                        }
                                                                                    }
                                                                                );
                                                                            } else {
                                                                                // si no existen crearlas
                                                                                var newUserAccounts = Object.assign({}, {
                                                                                    "id_user": JSONLoggedUser.id_user,
                                                                                    "last_acces_update": new Date().getTime()
                                                                                },
                                                                                    bodyAccounts);

                                                                                httpClient.post("account/?" + mLabAPIKey, newUserAccounts,
                                                                                    function (err, resMLab, body) {
                                                                                        console.log("Cuentas de usuario guardadas con éxito");
                                                                                        console.log(body);

                                                                                        deleteTSEC(resBBVAGT.headers.tsec);

                                                                                        res.status(201);
                                                                                        res.send(newUserAccounts);
                                                                                    }
                                                                                )
                                                                            }
                                                                        }
                                                                    }
                                                                );
                                                            }
                                                        }
                                                    );
                                                } else {
                                                    console.log("Error en la llamada al GT");
                                                    response = {
                                                        "msg": "Credenciales de BBVA incorrectas"
                                                    }
                                                    res.status(401);
                                                    res.send(response);
                                                }
                                            }
                                        }
                                    );
                                } else {
                                    response = {
                                        "msg": "El usuario no tiene credenciales de BBVA"
                                    }
                                    res.status(200);
                                    res.send(response);
                                }

                            } else {
                                // recogemos la información de BBDD
                                var queryExistAccountUser = "q={\"id_user\": " + req.query.id_user + "}&f={\"_id\": 0}";
                                console.log("Cliente creado: " + baseMlabURL + "account?" + queryExistAccountUser);

                                httpClient.get("account?" + queryExistAccountUser + "&" + mLabAPIKey,
                                    // resMlab - contiene toda la información de la API
                                    function (err, resMlab, bodyExistAccountUser) {
                                        if (err) {
                                            console.log("Error al obtener si el usuario tiene cuentas asociadas");
                                            console.log(err);
                                            response = {
                                                "msg": "Error obteniendo usuarios"
                                            }
                                            res.status(500);
                                            res.send(response);
                                        } else {
                                            if (bodyExistAccountUser.length > 0) {
                                                console.log("Se devuelven las cuentas de la BBDD");
                                                res.status(200);
                                                res.send(bodyExistAccountUser[0]);
                                            } else {
                                                // Si tiene las credenciales dadas de alta, recuperamos
                                                // la información, sino indicamos que no tiene cuentas
                                                if ((body[0].bbva != null) && (typeof (body[0].bbva) != "undefined")) {
                                                    bbva_user = body[0].bbva.user;
                                                    bbva_password = crypt.decipherText(body[0].bbva.password);

                                                    var httpClientBBVA = requestJSON.createClient(baseBBVA);
                                                    console.log("Cliente creado: " + baseBBVA + "TechArchitecture/grantingTickets/V02");

                                                    var postBody = '{' +
                                                        '"authentication":{' +
                                                        '"consumerID":"00000130",' +
                                                        '"authenticationData":[{' +
                                                        '"authenticationData":["' + bbva_password + '"],' +
                                                        '"idAuthenticationData":"password"' +
                                                        '}],' +
                                                        '"authenticationType":"02",' +
                                                        '"userID":"0019-0' + bbva_user.toUpperCase() + '"' + // tener en cuenta formato, rellenar con ceros
                                                        '}' +
                                                        '}';

                                                    httpClientBBVA.post("TechArchitecture/grantingTickets/V02",
                                                        JSON.parse(postBody),
                                                        function (err, resBBVAGT, bodyGT) {
                                                            if (err) {
                                                                console.log("Error en la llamada al GT");
                                                                console.log(err);
                                                                response = {
                                                                    "msg": "Error durante el proceso de autenticación en BBVA"
                                                                }
                                                                res.status(500);
                                                                res.send(response);
                                                            } else {
                                                                if ((resBBVAGT.headers.tsec != null) && (typeof (resBBVAGT.headers.tsec) != "undefined")) {
                                                                    console.log("bodyGT.user.id: " + bodyGT.user.id);
                                                                    console.log("bodyGT.user.alias: " + bodyGT.user.alias);
                                                                    console.log("bodyGT.user.person.id: " + bodyGT.user.person.id);

                                                                    console.log("resBBVAGT.headers.tsec");
                                                                    console.log(resBBVAGT.headers.tsec);

                                                                    httpClientBBVA.headers['tsec'] = resBBVAGT.headers.tsec;
                                                                    httpClientBBVA.get("psd2/v0/accounts?customerId=" + bodyGT.user.id,
                                                                        function (err, resBBVAAccounts, bodyAccounts) {
                                                                            if (err) {
                                                                                console.log("Error al obtener las cuentas del usuario");
                                                                                console.log(err);
                                                                                response = {
                                                                                    "msg": "Error al obtener las cuentas en BBVA"
                                                                                }

                                                                                deleteTSEC(resBBVAGT.headers.tsec);

                                                                                res.status(500);
                                                                                res.send(response);
                                                                            } else {
                                                                                // actualizamos la información en BBDD
                                                                                // debemos revisar si existe el registro
                                                                                var queryExistAccountUser = "q={\"id_user\": " + req.query.id_user + "}&f={\"_id\": 0}";
                                                                                console.log("Cliente creado: " + baseMlabURL + "account?" + queryExistAccountUser);

                                                                                httpClient.get("account?" + queryExistAccountUser + "&" + mLabAPIKey,
                                                                                    // resMlab - contiene toda la información de la API
                                                                                    function (err, resMlab, bodyExistAccountUser) {
                                                                                        if (err) {
                                                                                            console.log("Error al obtener si el usuario tiene cuentas asociadas");
                                                                                            console.log(err);
                                                                                            response = {
                                                                                                "msg": "Error obteniendo usuarios"
                                                                                            }

                                                                                            deleteTSEC(resBBVAGT.headers.tsec);

                                                                                            res.status(500);
                                                                                            res.send(response);
                                                                                        } else {
                                                                                            if (bodyExistAccountUser.length > 0) {
                                                                                                // si existe actualizarlo
                                                                                                var putBody = '{"$set":{' +
                                                                                                    '"last_acces_update": ' + new Date().getTime() + ',' +
                                                                                                    '"items": ' + JSON.stringify(bodyAccounts.items) +
                                                                                                    '}' +
                                                                                                    '}';

                                                                                                console.log("putBody");
                                                                                                console.log(putBody);

                                                                                                httpClient.put("account?" + queryExistAccountUser + "&" + mLabAPIKey,
                                                                                                    JSON.parse(putBody),
                                                                                                    function (err, resMlab, body) {
                                                                                                        if (err) {
                                                                                                            console.log("Error al guardar en Mlab los datos");
                                                                                                            console.log(err);
                                                                                                            response = [{
                                                                                                                "location": "body",
                                                                                                                "msg": "Error al actualizar los datos"
                                                                                                            }]

                                                                                                            deleteTSEC(resBBVAGT.headers.tsec);

                                                                                                            res.status(401);
                                                                                                            res.send(response);
                                                                                                        } else {
                                                                                                            console.log("Cuentas actualizadas correctamente");
                                                                                                            
                                                                                                            deleteTSEC(resBBVAGT.headers.tsec);
                                                                                                            
                                                                                                            res.status(200);

                                                                                                            var userAccounts = Object.assign({}, {
                                                                                                                "id_user": req.query.id_user,
                                                                                                                "last_acces_update": new Date().getTime()
                                                                                                            },
                                                                                                                bodyAccounts);

                                                                                                            res.send(userAccounts);
                                                                                                        }
                                                                                                    }
                                                                                                );
                                                                                            } else {
                                                                                                // si no existen crearlas
                                                                                                var newUserAccounts = Object.assign({}, {
                                                                                                    "id_user": JSONLoggedUser.id_user,
                                                                                                    "last_acces_update": new Date().getTime()
                                                                                                },
                                                                                                    bodyAccounts);

                                                                                                httpClient.post("account/?" + mLabAPIKey, newUserAccounts,
                                                                                                    function (err, resMLab, body) {
                                                                                                        console.log("Cuentas de usuario guardadas con éxito");
                                                                                                        console.log(body);

                                                                                                        deleteTSEC(resBBVAGT.headers.tsec);

                                                                                                        res.status(201);
                                                                                                        res.send(newUserAccounts);
                                                                                                    }
                                                                                                )
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                );
                                                                            }
                                                                        }
                                                                    );
                                                                } else {
                                                                    console.log("Error en la llamada al GT");
                                                                    response = {
                                                                        "msg": "Credenciales de BBVA incorrectas"
                                                                    }
                                                                    res.status(401);
                                                                    res.send(response);
                                                                }
                                                            }
                                                        }
                                                    );
                                                } else {
                                                    console.log("El cliente no tiene cuentas asociadas");
                                                    response = {
                                                        "msg": "El cliente no tiene cuentas asociadas"
                                                    }
                                                    res.status(200);
                                                    res.send(response);
                                                }
                                            }
                                        }
                                    }
                                );
                            }
                        }
                    }
                );
            } else {
                console.log("Identificador de usuario incorrecto");

                var response = [{
                    "msg": "Identificador de usuario incorrecto"
                }]
                res.status(400);
                res.send(response);
            }
        } else {
            console.log("Identificador de usuario incorrecto");

            var response = [{
                "msg": "Identificador de usuario incorrecto"
            }]
            res.status(400);
            res.send(response);
        }
    }
}

function getTransactionsAccountByIdV1(req, res) {
    console.log("GET /techu-bpjjsa/v1/accounts/:id_account");

    var errors = validationResult(req);

    if (!errors.isEmpty()) {
        res.status(400).json(errors.array());
    } else {
        console.log("id_account es " + req.params.id_account);

        var loggedUser = req.session.user;
        console.log("loggedUser");
        console.log(loggedUser);

        console.log("req.cookies.user_sid");
        console.log(req.cookies.user_sid);

        if (loggedUser && req.cookies.user_sid) {
            var JSONLoggedUser = JSON.parse(loggedUser);
            console.log("JSONLoggedUser.id_user");
            console.log(JSONLoggedUser.id_user);

            var httpClient = requestJSON.createClient(baseMlabURL);

            // Recuperamos si el usuario tiene o no credenciales en otros bancos
            var query = "q={\"id_user\": " + JSONLoggedUser.id_user + "}&f={\"_id\": 0, \"password\": 0}";
            console.log("Cliente creado: " + baseMlabURL + "user?" + query);

            httpClient.get("user?" + query + "&" + mLabAPIKey,
                // resMlab - contiene toda la información de la API
                function (err, resMlab, body) {
                    var response = {};
                    if (err) {
                        response = {
                            "msg": "Error obteniendo usuarios"
                        }
                        res.status(500);
                        res.send(response);
                    } else {
                        var bbva_user = null;
                        var bbva_password = null;
                        if ((body[0].bbva != null) && (typeof (body[0].bbva) != "undefined")) {
                            bbva_user = body[0].bbva.user;
                            bbva_password = crypt.decipherText(body[0].bbva.password);
                        }

                        // recogemos la de BBDD
                        var queryExistTxAccountUser = "q={\"id_user\": " + JSONLoggedUser.id_user + ", \"id_account\": \"" + req.params.id_account + "\"}&f={\"_id\": 0}";
                        console.log("Cliente creado: " + baseMlabURL + "transaction?" + queryExistTxAccountUser);

                        httpClient.get("transaction?" + queryExistTxAccountUser + "&" + mLabAPIKey,
                            // resMlab - contiene toda la información de la API
                            function (err, resMlab, bodyExistTxAccountUser) {
                                if (err) {
                                    console.log("Error al obtener las transacciones asociadas a la cuenta del usuario");
                                    console.log(err);
                                    response = {
                                        "msg": "Error obteniendo usuarios"
                                    }
                                    res.status(500);
                                    res.send(response);
                                } else {
                                    if (bodyExistTxAccountUser.length > 0) {
                                        console.log("Se devuelven las transacciones de la cuentas de la BBDD");
                                        res.status(200);
                                        res.send(bodyExistTxAccountUser[0]);
                                    } else {
                                        // si no hay datos, pero tiene credenciales, actualizamos
                                        var httpClientBBVA = requestJSON.createClient(baseBBVA);
                                        console.log("Cliente creado: " + baseBBVA + "TechArchitecture/grantingTickets/V02");

                                        var postBody = '{' +
                                            '"authentication":{' +
                                            '"consumerID":"00000130",' +
                                            '"authenticationData":[{' +
                                            '"authenticationData":["' + bbva_password + '"],' +
                                            '"idAuthenticationData":"password"' +
                                            '}],' +
                                            '"authenticationType":"02",' +
                                            '"userID":"0019-0' + bbva_user.toUpperCase() + '"' + // tener en cuenta formato, rellenar con ceros
                                            '}' +
                                            '}';

                                        httpClientBBVA.post("TechArchitecture/grantingTickets/V02",
                                            JSON.parse(postBody),
                                            function (err, resBBVAGT, bodyGT) {
                                                if (err) {
                                                    console.log("Error en la llamada al GT");
                                                    console.log(err);
                                                    response = {
                                                        "msg": "Error durante el proceso de autenticación en BBVA"
                                                    }
                                                    res.status(500);
                                                    res.send(response);
                                                } else {
                                                    if ((resBBVAGT.headers.tsec != null) && (typeof (resBBVAGT.headers.tsec) != "undefined")) {
                                                        console.log("bodyGT.user.id: " + bodyGT.user.id);
                                                        console.log("bodyGT.user.alias: " + bodyGT.user.alias);
                                                        console.log("bodyGT.user.person.id: " + bodyGT.user.person.id);

                                                        console.log("resBBVAGT.headers.tsec");
                                                        console.log(resBBVAGT.headers.tsec);

                                                        console.log("Cliente creado: " + baseBBVA + "psd2/v0/accounts/" + req.params.id_account + "/transactions?accountId=" + req.params.id_account);

                                                        httpClientBBVA.headers['tsec'] = resBBVAGT.headers.tsec;
                                                        httpClientBBVA.get("psd2/v0/accounts/" + req.params.id_account + "/transactions?accountId=" + req.params.id_account,
                                                            function (err, resBBVAAccounts, bodyAccounts) {
                                                                if (err) {
                                                                    console.log("Error al obtener los movimientos de la cuenta del usuario");
                                                                    console.log(err);
                                                                    response = {
                                                                        "msg": "Error al obtener los movimientos de la cuenta del usuario"
                                                                    }

                                                                    deleteTSEC(resBBVAGT.headers.tsec);

                                                                    res.status(500);
                                                                    res.send(response);
                                                                } else {
                                                                    console.log("Movimientos de la cuenta");
                                                                    console.log(bodyAccounts);

                                                                    // actualizamos la información en BBDD (no existe previamente)
                                                                    // para ello debemos recoger la información de la cuenta ppal.
                                                                    console.log("El usuario tiene credenciales de banco, pero el listado de movimientos está vacio inicialmente");
                                                                    console.log("Cliente creado: " + baseBBVA + "psd2/v0/accounts/" + req.params.id_account + "?accountId=" + req.params.id_account);

                                                                    httpClientBBVA.headers['tsec'] = resBBVAGT.headers.tsec;
                                                                    httpClientBBVA.get("psd2/v0/accounts/" + req.params.id_account + "?accountId=" + req.params.id_account,
                                                                        function (err, resBBVATXAccounts, bodyTXAccounts) {
                                                                            if (err) {
                                                                                console.log("Error al obtener el detalle de la cuenta");
                                                                                console.log(err);
                                                                                response = {
                                                                                    "msg": "Error al obtener el detalle de la cuenta"
                                                                                }

                                                                                deleteTSEC(resBBVAGT.headers.tsec);

                                                                                res.status(500);
                                                                                res.send(response);
                                                                            } else {
                                                                                console.log("bodyTXAccounts");
                                                                                console.log(bodyTXAccounts);

                                                                                if (bodyTXAccounts != null && typeof (bodyTXAccounts) != undefined && bodyTXAccounts != "") {
                                                                                    var newTXUserAccounts = Object.assign({}, {
                                                                                        "id_user": parseInt(JSONLoggedUser.id_user),
                                                                                        "id_account": req.params.id_account,
                                                                                        "iban": bodyTXAccounts.formats[2].number,
                                                                                        "product_name": bodyTXAccounts.product.name,
                                                                                        "current_balance": bodyTXAccounts.availableBalance.currentBalance.amount,
                                                                                    },
                                                                                        bodyAccounts);

                                                                                    console.log("newTXUserAccounts");
                                                                                    console.log(newTXUserAccounts);

                                                                                    httpClient.post("transaction/?" + mLabAPIKey, newTXUserAccounts,
                                                                                        function (err, resMLab, body) {
                                                                                            console.log("Listado de movimientos de cuentas guardadas con éxito");
                                                                                            console.log(body);

                                                                                            deleteTSEC(resBBVAGT.headers.tsec);

                                                                                            res.status(201);
                                                                                            res.send(newTXUserAccounts);
                                                                                        }
                                                                                    )
                                                                                } else {
                                                                                    console.log("Información de cuenta incorrecta");
                                                                                    response = {
                                                                                        "msg": "Información de cuenta incorrecta"
                                                                                    }
                                                                                    
                                                                                    deleteTSEC(resBBVAGT.headers.tsec);

                                                                                    res.status(401);
                                                                                    res.send(response);
                                                                                }
                                                                            }
                                                                        }
                                                                    );
                                                                }
                                                            }
                                                        );
                                                    } else {
                                                        console.log("Error en la llamada al GT");
                                                        response = {
                                                            "msg": "Credenciales de BBVA incorrectas"
                                                        }
                                                        res.status(401);
                                                        res.send(response);
                                                    }
                                                }
                                            }
                                        );
                                    }
                                }
                            }
                        );
                    }
                }
            );
        } else {
            console.log("Identificador de usuario incorrecto");

            var response = [{
                "msg": "Identificador de usuario incorrecto"
            }]
            res.status(400);
            res.send(response);
        }
    }
}

function updateTransactionsAccountById(id_user, id_account) {
    console.log("updateTransactionsAccountById");
    console.log("id_user: " + id_user);
    console.log("id_account: " + id_account);

    var httpClient = requestJSON.createClient(baseMlabURL);

    // Recuperamos si el usuario tiene o no credenciales en otros bancos
    var query = "q={\"id_user\": " + id_user + "}&f={\"_id\": 0, \"password\": 0}";
    console.log("Cliente creado: " + baseMlabURL + "user?" + query);

    httpClient.get("user?" + query + "&" + mLabAPIKey,
        // resMlab - contiene toda la información de la API
        function (err, resMlab, body) {
            var response = {};
            if (err) {
                response = {
                    "msg": "Error obteniendo usuarios"
                }
            } else {
                var bbva_user = null;
                var bbva_password = null;
                if ((body[0].bbva != null) && (typeof (body[0].bbva) != "undefined")) {
                    bbva_user = body[0].bbva.user;
                    bbva_password = crypt.decipherText(body[0].bbva.password);
                }

                // si tiene credenciales, actualizamos
                var httpClientBBVA = requestJSON.createClient(baseBBVA);
                console.log("Cliente creado: " + baseBBVA + "TechArchitecture/grantingTickets/V02");

                var postBody = '{' +
                    '"authentication":{' +
                    '"consumerID":"00000130",' +
                    '"authenticationData":[{' +
                    '"authenticationData":["' + bbva_password + '"],' +
                    '"idAuthenticationData":"password"' +
                    '}],' +
                    '"authenticationType":"02",' +
                    '"userID":"0019-0' + bbva_user.toUpperCase() + '"' + // tener en cuenta formato, rellenar con ceros
                    '}' +
                    '}';

                httpClientBBVA.post("TechArchitecture/grantingTickets/V02",
                    JSON.parse(postBody),
                    function (err, resBBVAGT, bodyGT) {
                        if (err) {
                            console.log("Error en la llamada al GT");
                            console.log(err);
                            response = {
                                "msg": "Error durante el proceso de autenticación en BBVA"
                            }
                        } else {
                            if ((resBBVAGT.headers.tsec != null) && (typeof (resBBVAGT.headers.tsec) != "undefined")) {
                                console.log("bodyGT.user.id: " + bodyGT.user.id);
                                console.log("bodyGT.user.alias: " + bodyGT.user.alias);
                                console.log("bodyGT.user.person.id: " + bodyGT.user.person.id);

                                console.log("resBBVAGT.headers.tsec");
                                console.log(resBBVAGT.headers.tsec);

                                console.log("Cliente creado: " + baseBBVA + "psd2/v0/accounts/" + id_account + "/transactions?accountId=" + id_account);

                                httpClientBBVA.headers['tsec'] = resBBVAGT.headers.tsec;
                                httpClientBBVA.get("psd2/v0/accounts/" + id_account + "/transactions?accountId=" + id_account,
                                    function (err, resBBVAAccounts, bodyAccounts) {
                                        if (err) {
                                            console.log("Error al obtener los movimientos de la cuenta del usuario");
                                            console.log(err);
                                            response = {
                                                "msg": "Error al obtener los movimientos de la cuenta del usuario"
                                            }
                                            deleteTSEC(resBBVAGT.headers.tsec);
                                        } else {
                                            console.log("Movimientos de la cuenta");
                                            console.log(bodyAccounts);

                                            // actualizamos la información en BBDD (no existe previamente)
                                            // para ello debemos recoger la información de la cuenta ppal.
                                            console.log("El usuario tiene credenciales de banco, pero el listado de movimientos está vacio inicialmente");
                                            console.log("Cliente creado: " + baseBBVA + "psd2/v0/accounts/" + id_account + "?accountId=" + id_account);

                                            httpClientBBVA.headers['tsec'] = resBBVAGT.headers.tsec;
                                            httpClientBBVA.get("psd2/v0/accounts/" + id_account + "?accountId=" + id_account,
                                                function (err, resBBVATXAccounts, bodyTXAccounts) {
                                                    if (err) {
                                                        console.log("Error al obtener el detalle de la cuenta");
                                                        console.log(err);
                                                        response = {
                                                            "msg": "Error al obtener el detalle de la cuenta"
                                                        }
                                                        deleteTSEC(resBBVAGT.headers.tsec);
                                                    } else {
                                                        console.log("bodyTXAccounts");
                                                        console.log(bodyTXAccounts);

                                                        if (bodyTXAccounts != null && typeof (bodyTXAccounts) != undefined && bodyTXAccounts != "") {

                                                            // Tenemos que revisar si existe la entrada a nivel de BBDD
                                                            // Si existe tenemos que actualizarla, sino crearla
                                                            var queryExistTxAccountUser = "q={\"id_user\": " + id_user + ", \"id_account\": \"" + id_account + "\"}&f={\"_id\": 0}";
                                                            console.log("Cliente creado: " + baseMlabURL + "transaction?" + queryExistTxAccountUser);

                                                            httpClient.get("transaction?" + queryExistTxAccountUser + "&" + mLabAPIKey,
                                                                // resMlab - contiene toda la información de la API
                                                                function (err, resMlab, bodyExistTxAccountUser) {
                                                                    if (err) {
                                                                        console.log("Error al obtener las transacciones asociadas a la cuenta del usuario");
                                                                        console.log(err);
                                                                        response = {
                                                                            "msg": "Error obteniendo usuarios"
                                                                        }
                                                                        deleteTSEC(resBBVAGT.headers.tsec);
                                                                    } else {
                                                                        if (bodyExistTxAccountUser != null && bodyExistTxAccountUser != "") {
                                                                            console.log("actualiza cuenta vieja")

                                                                            var putBody = '{"$set":{' +
                                                                                '"product_name": "' + bodyTXAccounts.product.name + '",' +
                                                                                '"current_balance": ' + bodyTXAccounts.availableBalance.currentBalance.amount + ',' +
                                                                                '"transactions": ' + JSON.stringify(bodyAccounts.transactions) +
                                                                                '}' +
                                                                                '}';

                                                                            console.log("putBody");
                                                                            console.log(putBody);

                                                                            httpClient.put("transaction?" + queryExistTxAccountUser + "&" + mLabAPIKey,
                                                                                JSON.parse(putBody),
                                                                                function (err, resMlab, body) {
                                                                                    if (err) {
                                                                                        console.log("Error al actualizar el listado de movimientos");
                                                                                        console.log(err);
                                                                                        response = [{
                                                                                            "location": "body",
                                                                                            "msg": "Error al actualizar los datos"
                                                                                        }]
                                                                                        deleteTSEC(resBBVAGT.headers.tsec);
                                                                                    } else {
                                                                                        console.log("Cuentas actualizadas correctamente");
                                                                                        deleteTSEC(resBBVAGT.headers.tsec);
                                                                                    }
                                                                                }
                                                                            );
                                                                        } else {
                                                                            console.log("crea cuenta nueva")

                                                                            // si no existe lo creamos
                                                                            var newTXUserAccounts = Object.assign({}, {
                                                                                "id_user": parseInt(id_user),
                                                                                "id_account": id_account,
                                                                                "iban": bodyTXAccounts.formats[2].number,
                                                                                "product_name": bodyTXAccounts.product.name,
                                                                                "current_balance": bodyTXAccounts.availableBalance.currentBalance.amount,
                                                                            },
                                                                                bodyAccounts);

                                                                            httpClient.post("transaction/?" + mLabAPIKey, newTXUserAccounts,
                                                                                function (err, resMLab, body) {
                                                                                    console.log("Listado de movimientos de cuentas guardadas con éxito");
                                                                                    console.log(body);
                                                                                    deleteTSEC(resBBVAGT.headers.tsec);
                                                                                }
                                                                            )
                                                                        }
                                                                    }
                                                                }
                                                            );
                                                        } else {
                                                            console.log("Información de cuenta incorrecta");
                                                            response = {
                                                                "msg": "Información de cuenta incorrecta"
                                                            }
                                                            deleteTSEC(resBBVAGT.headers.tsec);
                                                        }
                                                    }
                                                }
                                            );
                                        }
                                    }
                                );
                            } else {
                                console.log("Error en la llamada al GT");
                                response = {
                                    "msg": "Credenciales de BBVA incorrectas"
                                }
                            }
                        }
                    }
                );
            }
        }
    );
}

function deleteTSEC(tsec) {

    var httpClientBBVA = requestJSON.createClient(baseBBVA);
    console.log("Cliente creado: " + baseBBVA + "TechArchitecture/grantingTickets/V02");
    
    httpClientBBVA.headers['tsec'] = tsec;
    httpClientBBVA.delete("TechArchitecture/grantingTickets/V02",
        function (err, resMlab, body) {
            if (err) {
                console.log("Error al eliminar el TSEC");
                console.log(err);
            } else {
                console.log("TSEC eliminado correctamente");
                console.log(body);
            }
        }
    );
}

module.exports.getAccountsByIdV1 = getAccountsByIdV1;
module.exports.getTransactionsAccountByIdV1 = getTransactionsAccountByIdV1