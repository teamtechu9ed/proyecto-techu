require('dotenv').config();

const crypt = require('../crypt');
const promise = require('promise');
const requestJSON = require('request-json');
const { check, validationResult } = require('express-validator/check');

const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;
const baseMlabURL = "https://api.mlab.com/api/1/databases/techu-bpjjsa/collections/";

/*
GET
Retrieve information. 
GET requests must be safe and idempotent, meaning regardless of how many times it repeats with the same parameters, 
the results are the same. They can have side effects, but the user doesn't expect them, so they cannot be critical 
to the operation of the system. Requests can also be partial or conditional.
*/
function getUsersV1(req, res) {
    console.log("GET /techu-bpjjsa/v1/users");

    // URL BASE
    var httpClient = requestJSON.createClient(baseMlabURL);
    console.log("Cliente creado: " + baseMlabURL + "user?");

    httpClient.get("user?" + mLabAPIKey,
        // resMlab - contiene toda la información de la API
        function (err, resMlab, body) {
            if (err) {
                res.status(500);
            }

            var response = !err ? body : {
                "msg": "Error obteniendo usuarios"
            }
            res.send(response);
        }
    );
}


/*
GET
Retrieve information. 
GET requests must be safe and idempotent, meaning regardless of how many times it repeats with the same parameters, 
the results are the same. They can have side effects, but the user doesn't expect them, so they cannot be critical 
to the operation of the system. Requests can also be partial or conditional.
*/
function getUserByIdV1(req, res) {
    console.log("GET /techu-bpjjsa/v1/users/:id_user");

    var errors = validationResult(req);

    if (!errors.isEmpty()) {
        res.status(400).json(errors.array());
    } else {
        console.log("id_user es " + req.params.id_user);

        var loggedUser = req.session.user;
        console.log("loggedUser");
        console.log(loggedUser);

        console.log("req.cookies.user_sid");
        console.log(req.cookies.user_sid);

        if (loggedUser && req.cookies.user_sid) {
            var JSONLoggedUser = JSON.parse(loggedUser);
            console.log("JSONLoggedUser.id_user");
            console.log(JSONLoggedUser.id_user);

            if (JSONLoggedUser.id_user == req.params.id_user) {
                console.log("Identificador de usuario correcto")

                // URL BASE
                var httpClient = requestJSON.createClient(baseMlabURL);

                // Excluimos el id interno de Mlab y la contraseña 
                var query = "q={\"id_user\": " + req.params.id_user + "}&f={\"_id\": 0, \"password\": 0, \"bbva.password\": 0}";

                console.log("Cliente creado: " + baseMlabURL + "user?" + query);

                httpClient.get("user?" + query + "&" + mLabAPIKey,
                    // resMlab - contiene toda la información de la API
                    function (err, resMlab, body) {
                        if (err) {
                            var response = {
                                "msg": "Error obteniendo usuarios"
                            }
                            res.status(500);
                        } else {
                            if (body.length > 0) {
                                var response = body[0];

                                // actualizamos en BBDD
                                var putBody = '{"$set":{"last_acces_date": ' + new Date().getTime() + '}}';

                                httpClient.put("user?" + query + "&" + mLabAPIKey,
                                    JSON.parse(putBody),
                                    function (err, resMlab, body) {
                                        if (err) {
                                            resultado = [{
                                                "location": "body",
                                                "msg": "Error al actualizar la fecha de acceso"
                                            }]
                                            res.status(401);
                                        }
                                    }
                                )
                            } else {
                                var response = {
                                    "msg": "Usuario no encontrado"
                                }
                                res.status(404);
                            }
                        }

                        res.send(response);
                    }
                );
            } else {
                console.log("Identificador de usuario incorrecto");

                var response = [{
                    "msg": "Identificador de usuario incorrecto"
                }]
                res.status(400);
                res.send(response);
            }
        } else {
            console.log("Identificador de usuario incorrecto");

            var response = [{
                "msg": "Identificador de usuario incorrecto"
            }]
            res.status(400);
            res.send(response);
        }
    }
}

function alreadyHaveEmail(email) {
    return new Promise(function (resolve, reject) {
        console.log("Comprobamos si existe el email: " + email);

        var response = email;
        var httpClient = requestJSON.createClient(baseMlabURL);

        // comprobamos que no exista un mismo email ya creado
        var queryEmail = "q={\"email\":\"" + email + "\"}";
        console.log("Cliente creado: " + baseMlabURL + "user?" + queryEmail);

        httpClient.get("user?" + queryEmail + "&" + mLabAPIKey,
            function (err, resMlab, res) {
                if (err) {
                    console.log("err: " + err);
                    return reject(err) // rejects the promise with `err` as the reason
                } else {
                    if (res.length == 0) {
                        response = false;
                    } else {
                        console.log("El email ya existe en la plataforma");
                    }

                    resolve(response);
                }
            }
        );
    })
}

/*
POST
Request that the resource at the URI do something with the provided entity. 
Often POST is used to create a new entity, but it can also be used to update an entity.
*/
function createUserV1(req, res) {
    console.log("POST /techu-bpjjsa/v1/users")

    var errors = validationResult(req);

    if (!errors.isEmpty()) {
        res.status(400).json(errors.array());
    } else {
        var usersLen = 0;

        // URL BASE
        var httpClient = requestJSON.createClient(baseMlabURL);

        // obtenemos el ID
        var queryID = "s={\"id_user\": -1}&l=-1";
        console.log("Cliente creado: " + baseMlabURL + "user?" + queryID);

        httpClient.get("user?" + queryID + "&" + mLabAPIKey,
            function (err, resMlab, body) {
                if (!err) {
                    if (body.length > 0) {
                        usersLen = body[0].id_user + 1;
                        console.log("El primer identificador libre es: " + usersLen);
                    }
                }

                var newUser = {};

                newUser.id_user = usersLen;
                newUser.display_name = req.body.display_name;
                newUser.password = crypt.hash(req.body.password);
                newUser.last_name = req.body.last_name;
                newUser.email = req.body.email;
                newUser.last_acces_date = null;
                newUser.register_date = new Date().getTime();
                newUser.picture = null;

                //console.log("user: " + JSON.stringify(newUser));

                httpClient.post("user/?" + mLabAPIKey, newUser,
                    function (err, resMLab, body) {
                        console.log("Usuario guardado con éxito");
                        console.log(body);
                        res.status(201).send({ "msg": "usuario guardado" });
                    }
                )
            }
        );
    }
}

/*
    PATCH
    Update only the specified fields of an entity at a URI. A PATCH request is neither safe nor idempotent (RFC 5789).
    That's because a PATCH operation cannot ensure the entire resource has been updated.
*/
function modifyUserV1(req, res) {
    console.log("PATCH /techu-bpjjsa/v1/users/:id_user")

    var errors = validationResult(req);

    if (!errors.isEmpty()) {
        res.status(400).json(errors.array());
    } else {
        console.log("id_user es " + req.params.id_user);

        var loggedUser = req.session.user;
        console.log("loggedUser");
        console.log(loggedUser);

        console.log("req.cookies.user_sid");
        console.log(req.cookies.user_sid);

        if (loggedUser && req.cookies.user_sid) {
            var JSONLoggedUser = JSON.parse(loggedUser);
            console.log("JSONLoggedUser.id_user");
            console.log(JSONLoggedUser.id_user);

            if (JSONLoggedUser.id_user == req.params.id_user) {
                console.log("Identificador de usuario correcto")

                var resultado = [];

                // URL BASE
                var httpClient = requestJSON.createClient(baseMlabURL);

                var queryEmail = "q={\"email\":\"" + req.body.email + "\"}";
                console.log("Cliente creado: " + baseMlabURL + "user?" + queryEmail);

                httpClient.get("user?" + queryEmail + "&" + mLabAPIKey,
                    function (err, resMlab, body) {
                        if (err) {
                            console.log("err: " + err);
                            return reject(err) // rejects the promise with `err` as the reason
                        } else {
                            if (body.length != 0 && body[0].id_user != req.params.id_user) {
                                // existe el email en la plataforma
                                // pero el identificador es distinto que el actual (hay problema)
                                resultado = [{
                                    "location": "body",
                                    "param": "email",
                                    "msg": "El email ya existe en la plataforma"
                                }]
                                res.status(401);
                                res.send(resultado);
                            } else {
                                console.log("El email nuevo no existe en la plataforma, o bien es el del mismo usuario");

                                // actualizamos en BBDD
                                var query = "q={\"id_user\": " + req.params.id_user + "}";

                                var putBody = '{' +
                                    '"$set": {' +
                                    '"display_name": "' + req.body.display_name + '",' +
                                    '"last_name": "' + req.body.last_name + '",' +
                                    ((req.body.password) ? ('"password": "' + crypt.hash(req.body.password) + '",') : '') +
                                    '"email": "' + req.body.email + '"' +
                                    '}' +
                                    '}';

                                console.log("putBody");
                                console.log(JSON.parse(putBody));

                                httpClient.put("user?" + query + "&" + mLabAPIKey,
                                    JSON.parse(putBody),
                                    function (err, resMlab, body) {
                                        if (err) {
                                            resultado = [{
                                                "location": "body",
                                                "msg": "Error al actualizar los datos"
                                            }]
                                            res.status(401);
                                        } else {
                                            console.log("Usuario modificado con éxito");
                                            console.log(body);
                                            resultado = [{
                                                "location": "body",
                                                "msg": "Usuario modificado con éxito"
                                            }]
                                            res.status(200);
                                        }
                                        res.send(resultado);
                                    }
                                )
                            }
                        }
                    }
                );
            } else {
                console.log("Identificador de usuario incorrecto");

                var response = [{
                    "msg": "Identificador de usuario incorrecto"
                }]
                res.status(400);
                res.send(response);
            }
        } else {
            console.log("Identificador de usuario incorrecto");

            var response = [{
                "msg": "Identificador de usuario incorrecto"
            }]
            res.status(400);
            res.send(response);
        }
    }
}

function modifyUserBankCredentialsV1(req, res) {
    console.log("PATCH /techu-bpjjsa/v1/users/:id_user/bankCredentials");

    var errors = validationResult(req);

    if (!errors.isEmpty()) {
        res.status(400).json(errors.array());
    } else {
        console.log("id_user es " + req.params.id_user);

        var loggedUser = req.session.user;
        console.log("loggedUser");
        console.log(loggedUser);

        console.log("req.cookies.user_sid");
        console.log(req.cookies.user_sid);

        if (loggedUser && req.cookies.user_sid) {
            var JSONLoggedUser = JSON.parse(loggedUser);
            console.log("JSONLoggedUser.id_user");
            console.log(JSONLoggedUser.id_user);

            if (JSONLoggedUser.id_user == req.params.id_user) {
                console.log("Identificador de usuario correcto")

                var resultado = [];
                var httpClient = requestJSON.createClient(baseMlabURL);

                // actualizamos en BBDD
                var query = "q={\"id_user\": " + req.params.id_user + "}";

                var putBody = '{' +
                        '"$set": {' +
                            '"bbva": {' +
                                '"user": "' + req.body.bbva_user + '",' +
                                '"password": "' + crypt.cipherText(req.body.bbva_password) + '"' +
                            '}' +
                        '}' +
                    '}';

                console.log("putBody");
                console.log(putBody);

                httpClient.put("user?" + query + "&" + mLabAPIKey,
                    JSON.parse(putBody),
                    function (err, resMlab, body) {
                        if (err) {
                            resultado = [{
                                "location": "body",
                                "msg": "Error al actualizar los datos"
                            }]
                            res.status(401);
                        } else {
                            console.log("Usuario modificado con éxito");
                            console.log(body);
                            resultado = [{
                                "location": "body",
                                "msg": "Usuario modificado con éxito"
                            }]
                            res.status(200);
                        }
                        res.send(resultado);
                    }
                )
            } else {
                console.log("Identificador de usuario incorrecto");

                var response = [{
                    "msg": "Identificador de usuario incorrecto"
                }]
                res.status(400);
                res.send(response);
            }
        } else {
            console.log("Identificador de usuario incorrecto");

            var response = [{
                "msg": "Identificador de usuario incorrecto"
            }]
            res.status(400);
            res.send(response);
        }
    }
}

function deleteUsersV1(req, res) {
    console.log("DELETE /techu-bpjjsa/v1/users/:id_user");

    var errors = validationResult(req);

    if (!errors.isEmpty()) {
        res.status(400).json(errors.array());
    } else {
        console.log("id_user es " + req.params.id_user);

        var loggedUser = req.session.user;
        console.log("loggedUser");
        console.log(loggedUser);

        console.log("req.cookies.user_sid");
        console.log(req.cookies.user_sid);

        if (loggedUser && req.cookies.user_sid) {
            var JSONLoggedUser = JSON.parse(loggedUser);
            console.log("JSONLoggedUser.id_user");
            console.log(JSONLoggedUser.id_user);

            if (JSONLoggedUser.id_user == req.params.id_user) {
                console.log("Identificador de usuario correcto")

                var httpClient = requestJSON.createClient(baseMlabURL);
                var queryArray = [];
                var query = "q={\"id_user\": " + req.params.id_user + "}";

                console.log("Cliente creado: " + baseMlabURL + "user?" + JSON.stringify(queryArray));

                httpClient.put("user?" + query + "&" + mLabAPIKey, queryArray,
                    function (err, resMLab, body) {
                        if (err) {
                            var response = [{
                                "msg": "Error obteniendo usuarios"
                            }]
                            res.status(500);
                        } else {
                            // debemos borrar las cuentas del usuario si tiene
                            console.log("Cliente creado: " + baseMlabURL + "account?" + JSON.stringify(queryArray));

                            httpClient.put("account?" + query + "&" + mLabAPIKey, queryArray,
                                function (err, resMLab, body) {
                                    if (err) console.log("Error borrando las cuentas asociadas al usuario");
                                               
                                    console.log("Usuario borrado con éxito");
                                    console.log(body);
                                    res.status(201).send({ "msg": "usuario borrado" });
                                }
                            );
                        }
                    }
                )
            } else {
                console.log("Identificador de usuario incorrecto");

                var response = [{
                    "msg": "Identificador de usuario incorrecto"
                }]
                res.status(400);
                res.send(response);
            }
        } else {
            console.log("Identificador de usuario incorrecto");

            var response = [{
                "msg": "Identificador de usuario incorrecto"
            }]
            res.status(400);
            res.send(response);
        }
    }
}

function deleteUserBankCredentialsV1(req, res) {
    console.log("DELETE /techu-bpjjsa/v1/users/:id_user/bankCredentials");

    var errors = validationResult(req);

    if (!errors.isEmpty()) {
        res.status(400).json(errors.array());
    } else {
        console.log("id_user es " + req.params.id_user);

        var loggedUser = req.session.user;
        console.log("loggedUser");
        console.log(loggedUser);

        console.log("req.cookies.user_sid");
        console.log(req.cookies.user_sid);

        if (loggedUser && req.cookies.user_sid) {
            var JSONLoggedUser = JSON.parse(loggedUser);
            console.log("JSONLoggedUser.id_user");
            console.log(JSONLoggedUser.id_user);

            if (JSONLoggedUser.id_user == req.params.id_user) {
                console.log("Identificador de usuario correcto")

                var resultado = [];
                var httpClient = requestJSON.createClient(baseMlabURL);

                // actualizamos en BBDD
                var query = "q={\"id_user\": " + req.params.id_user + "}";

                var putBody = '{' +
                    '"$unset": {' +
                        '"bbva": ""' +
                    '}' +
                '}';

                console.log("putBody");
                console.log(putBody);

                httpClient.put("user?" + query + "&" + mLabAPIKey,
                    JSON.parse(putBody),
                    function (err, resMlab, body) {
                        if (err) {
                            resultado = [{
                                "location": "body",
                                "msg": "Error al actualizar los datos"
                            }]
                            res.status(401);
                        } else {
                            console.log("Usuario modificado con éxito");
                            console.log(body);
                            resultado = [{
                                "location": "body",
                                "msg": "Usuario modificado con éxito"
                            }]
                            res.status(200);
                        }
                        res.send(resultado);
                    }
                )
            } else {
                console.log("Identificador de usuario incorrecto");

                var response = [{
                    "msg": "Identificador de usuario incorrecto"
                }]
                res.status(400);
                res.send(response);
            }
        } else {
            console.log("Identificador de usuario incorrecto");

            var response = [{
               "msg": "Identificador de usuario incorrecto"
            }]
            res.status(400);
            res.send(response);
        }
    }
}

module.exports.getUsersV1 = getUsersV1;
module.exports.getUserByIdV1 = getUserByIdV1;
module.exports.createUserV1 = createUserV1;
module.exports.deleteUsersV1 = deleteUsersV1;
module.exports.modifyUserV1 = modifyUserV1
module.exports.modifyUserBankCredentialsV1 = modifyUserBankCredentialsV1
module.exports.deleteUserBankCredentialsV1 = deleteUserBankCredentialsV1

module.exports.alreadyHaveEmail = alreadyHaveEmail