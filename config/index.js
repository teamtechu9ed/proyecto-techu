require('dotenv').config();

var path = require('path');
const uuidv4 = require('uuid/v4');
const express = require('express');
var session = require('express-session');
const bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');

const port = process.env.PORT || 3000;
const cookieDomain = process.env.URL_PROYECTO_BACK_TECHU || "localhost";
const accessControlAllowOrigin = process.env.URL_PROYECTO_FRONT_TECHU || "http://localhost:8081";

var enableCORS = function (req, res, next) {
    res.set("Access-Control-Allow-Origin", accessControlAllowOrigin);
    res.set("Access-Control-Allow-Methods", "GET, POST, HEAD, OPTIONS, PUT, DELETE, PATCH");
    res.set("Access-Control-Allow-Headers", "Content-Type, Accept, Set-Cookie, *");
    res.set("Access-Control-Allow-Credentials", "true");
    res.set("Access-Control-Request-Headers", "*");
    res.set("Access-Control-Expose-Headers", "*");
    // Se deberá habilitar la cabecera del token JWT (si empieza por X- es una cabecera que es propietaria)
    next();
}

function genuuid(){
    var random = uuidv4()
    console.log("genuuid: " + random);
    return random;
}

module.exports = function (app) {
    app.listen(port);
    app.use(express.json());

    // Configure Middleware
    app.use(bodyParser.urlencoded({ extended: true }));

    // permite la invocación desde distintos dominios
    app.use(enableCORS);

    // initialize cookie-parser to allow us access the cookies stored in the browser. 
    app.use(cookieParser());

    app.use(session({
        key: 'user_sid',
        secret: process.env.SESSION_SECRET,
        /*
        genid: function (req) {
            return genuuid() // use UUIDs for session IDs
        },
        */
        resave: false,
        saveUninitialized: false, //true
        cookie: {
            expires: 86400000,
            domain: cookieDomain
            /*
            secure: true,
            maxAge: 60000,
            httpOnly: true
            */
        }
        //rolling: true
    }))

    // This middleware will check if user's cookie is still saved in browser and user is not set, then automatically log the user out.
    // This usually happens when you stop your express server after login, your cookie still remains saved in the browser.
    app.use((req, res, next) => {
        if (req.cookies.user_sid && !req.session.user) {
            res.clearCookie('user_sid');        
        }
        next();
    });

    console.log("API escuchando en el puerto: " + port);
}