// config/passport.js

// load all the things we need
var LocalStrategy = require('passport-local').Strategy;
var FacebookStrategy = require('passport-facebook').Strategy;
var TwitterStrategy = require('passport-twitter').Strategy;

var GoogleStrategy = require('passport-google-oauth').OAuth2Strategy;

// load up the user model
var User = require('../app/models/user');

// load the auth variables
var configAuth = require('./auth');

module.exports = function (passport) {

    // used to serialize the user for the session
    passport.serializeUser(function (user, done) {
        done(null, user.id);
    });

    // used to deserialize the user
    passport.deserializeUser(function (id, done) {
        User.findById(id, function (err, user) {
            done(err, user);
        });
    });

    // code for login (use('local-login', new LocalStategy))
    // code for signup (use('local-signup', new LocalStategy))
    // code for facebook (use('facebook', new FacebookStrategy))
    // code for twitter (use('twitter', new TwitterStrategy))

    // =========================================================================
    // GOOGLE ==================================================================
    // =========================================================================

    // https://cloud.google.com/docs/authentication/
    // https://cloud.google.com/docs/authentication/end-user (paso a paso)

    // https://console.cloud.google.com/home/dashboard?project=techu-bpjjsa

    // Para sacar los datos a configurar a nivel de app
    // https://console.cloud.google.com/apis/credentials/oauthclient/641141537741-j453i5am55hgbpbl7vmndg08sa8hfask.apps.googleusercontent.com?project=techu-bpjjsa&folder&organizationId=718192113432

    // Como debe ser la pantalla de consentimiento
    // https://console.cloud.google.com/apis/credentials/consent?_ga=2.187135400.-1078295645.1544987755&_gac=1.217085284.1547299417.Cj0KCQiAvebhBRD5ARIsAIQUmnmdgiEloeIYcrlVfQh5mSj5OY5n8_ctOiWcvZ8FtqeOsN-r76S00d0aAsthEALw_wcB&project=techu-bpjjsa&folder&organizationId=718192113432

    // SCOPES
    // email    - https://www.googleapis.com/auth/userinfo.email
    // profile  - https://www.googleapis.com/auth/userinfo.profile
    // openid   - https://www.googleapis.com/auth/plus.me

    passport.use(new GoogleStrategy({
        clientID: configAuth.googleAuth.clientID,
        clientSecret: configAuth.googleAuth.clientSecret,
        callbackURL: configAuth.googleAuth.callbackURL,
    },
        function (token, refreshToken, profile, done) {

            // make the code asynchronous
            // User.findOne won't fire until we have all our data back from Google
            process.nextTick(function () {

                // try to find the user based on their google id
                User.findOne({ 'google.id': profile.id }, function (err, user) {
                    if (err)
                        console.log("Se ha producido un error en la autenticación");
                        console.log(err);
                        return done("Se ha producido un error en la autenticación");

                    if (user) {

                        // if a user is found, log them in
                        return done(null, user);
                    } else {
                        // if the user isnt in our database, create a new user
                        var newUser = new User();

                        // set all of the relevant information
                        newUser.google.id = profile.id;
                        newUser.google.token = token;
                        newUser.google.name = profile.displayName;
                        newUser.google.email = profile.emails[0].value; // pull the first email

                        // save the user
                        newUser.save(function (err) {
                            if (err){
                                console.log("Se ha producido un error en la autenticación");
                                console.log(err);  
                                throw err;
                            }
                            return done(null, newUser);
                        });
                    }
                });
            });
        }));
};