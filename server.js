console.log("Inicializando TechU BPJJSA");

// Disponibilizamos el framework de express y lo inicializamos
const express = require('express'),
    //    passportStrategy = require('./config/passportStrategy'),
    //    passport = require('passport'),
    //    routes = require('./app/routes'),
    config = require('./config/index'),
    { check, validationResult } = require('express-validator/check');

const userController = require('./controllers/UserController');
const authController = require('./controllers/AuthController');
const accountController = require('./controllers/AccountController');

const app = express();
config(app);

// routes(app, passport);
// app.use(passport.initialize());
// app.use(passport.session());
// passportStrategy(passport); 

app.get('/techu-bpjjsa/v1/hello',
    function (req, res) {
        console.log("GET /techu-bpjjsa/v1/hello");
        res.send({ "msg": "Hola desde API TechU" });
    }
);

/***********************
/ GESTION DE USUARIOS  /
***********************/
//app.get('/techu-bpjjsa/v1/users', userController.getUsersV1);
app.get('/techu-bpjjsa/v1/users/:id_user', [
    check('id_user').isNumeric().withMessage('El identificador del usuario debe ser numérico'),
], userController.getUserByIdV1);
app.post('/techu-bpjjsa/v1/users', [
    check('display_name').isLength({ min: 3 }).withMessage('El nombre de usuario es obligatorio'),
    check('password').isLength({ min: 4 }).withMessage('La contraseña debe tener al menos 4 carácteres'),
    check('email').isEmail().withMessage('Formato de email incorrecto'),
    check('email')
        .custom((value, { req, loc, path }) => {
            var response = userController.alreadyHaveEmail(value).then(function (res) {
                // el mensaje que salta si no cumple la condición salta cuando la respuesta es false
                // de cara a que sí existe ese email sale el mensaje negamos la respuesta
                return !res;
            });

            return response;
        }).withMessage("El email ya existe en la plataforma")
],
    userController.createUserV1);
app.delete('/techu-bpjjsa/v1/users/:id_user', userController.deleteUsersV1);
app.patch('/techu-bpjjsa/v1/users/:id_user', [
    check('display_name').isLength({ min: 3 }).withMessage('El nombre de usuario es obligatorio'),
    check('password').custom((value, { req, loc, path }) => {
        var response = false;

        if (value) {
            var len = value.length;
            if (len < 4) {
                response = true;
            }
        }

        return !response;
    }).withMessage('La contraseña debe tener al menos 4 carácteres'),
    check('bbva_password').custom((value, { req, loc, path }) => {
        var response = false;

        if (value) {
            var len = value.length;
            if (len < 4) {
                response = true;
            } else if (len > 6) {
                response = true;
            }
        }

        return !response;
    }).withMessage('La contraseña debe tener entre 4 y 6 carácteres'),
    check('email').isEmail().withMessage('Formato de email incorrecto')
],
    userController.modifyUserV1);


app.patch('/techu-bpjjsa/v1/users/:id_user', [
    check('display_name').isLength({ min: 3 }).withMessage('El nombre de usuario es obligatorio'),
    check('password').custom((value, { req, loc, path }) => {
        var response = false;

        if (value) {
            var len = value.length;
            if (len < 4) {
                response = true;
            }
        }

        return !response;
    }).withMessage('La contraseña debe tener al menos 4 carácteres'),
    check('email').isEmail().withMessage('Formato de email incorrecto')
],
    userController.modifyUserV1);

app.patch('/techu-bpjjsa/v1/users/:id_user/bankCredentials', [
    check('bbva_user').isLength({ min: 3 }).withMessage('El nombre de usuario es obligatorio'),
    check('bbva_password').custom((value, { req, loc, path }) => {
        var response = false;

        if (value) {
            var len = value.length;
            if (len < 4) {
                response = true;
            } else if (len > 6) {
                response = true;
            }
        }

        return !response;
    }).withMessage('La contraseña debe tener entre 4 y 6 carácteres'),
],
    userController.modifyUserBankCredentialsV1);
app.delete('/techu-bpjjsa/v1/users/:id_user/bankCredentials', userController.deleteUserBankCredentialsV1);

/*****************
/ AUTENTICACION  /
*****************/
app.post('/techu-bpjjsa/v1/login', [
    check('password').isLength({ min: 4 }).withMessage('La contraseña debe tener al menos 4 carácteres'),
    check('email').isEmail().withMessage('Formato de email incorrecto'),
],
    authController.postLoginV1);
app.post('/techu-bpjjsa/v1/logout', authController.postLogoutV1);
app.post('/techu-bpjjsa/v1/resetPassword', [
    check('email').isEmail().withMessage('Formato de email incorrecto'),
], authController.postResetPasswordUserV1);
app.patch('/techu-bpjjsa/v1/resetPassword', [
    check('email').isEmail().withMessage('Formato de email incorrecto'),
    check('token').isLength({ min: 60, max: 60 }).withMessage('Formato de token incorrecto'),
    check('password').isLength({ min: 4 }).withMessage('La contraseña debe tener al menos 4 carácteres'),
], authController.patchResetPasswordUserV1);

/**********/
/* GOOGLE */
/**********/
app.get('/techu-bpjjsa/v1/auth/google/authorize', authController.getOAuthURL);
app.get('/techu-bpjjsa/v1/auth/google/callback', authController.getOAuthCallback);

/*
// route for handling 404 requests(unavailable routes)
app.use(function (req, res, next) {
    var resultado = [{
        "msg": "Recurso no encontrado"
    }]
    res.status(404).send(resultado);
});
*/

/*********************
/ GESTION DE CUENTAS /
*********************/

/* !!!!!!!!! El id_user debe cogerlo del usurio logado, no es un parámetro que mete el usuario */
app.get('/techu-bpjjsa/v1/accounts', accountController.getAccountsByIdV1);
app.get('/techu-bpjjsa/v1/accounts/:id_account', accountController.getTransactionsAccountByIdV1);
