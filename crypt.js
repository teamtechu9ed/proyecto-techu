require('dotenv').config();

var CryptoJS = require("crypto-js");
const bcrypt = require('bcrypt-nodejs');

function hash(data) {
  console.log("Hashing data");
  var salt = bcrypt.genSaltSync();
  return bcrypt.hashSync(data, salt);
}

function checkPassword(passwordInPlainText, passwordHashed) {
  console.log("checkPassword");
  return bcrypt.compareSync(passwordInPlainText, passwordHashed);
}

function cipherText(message) {
  console.log("cipherText");

  // Encrypt
  var ciphertext = CryptoJS.AES.encrypt(message, process.env.CIPHER_KEY);
  console.log("resultado: " + ciphertext.toString());

  return ciphertext.toString();
}

function decipherText(message) {
  console.log("decipherText");
  console.log("message: " + message);

  // Decrypt
  var bytes = CryptoJS.AES.decrypt(message, process.env.CIPHER_KEY);
  var plaintext = bytes.toString(CryptoJS.enc.Utf8);

  return plaintext;
}

module.exports.hash = hash;
module.exports.checkPassword = checkPassword;
module.exports.cipherText = cipherText;
module.exports.decipherText = decipherText;