const mocha =require('mocha');
const chai =require('chai');
const chaihttp =require('chai-http');

chai.use(chaihttp);

var should = chai.should();

describe ('First test',
  function() {
    it('Test that MoneyMoney works', function(done) {
        chai.request('https://techu-techu-bpjjsa.7e14.starter-us-west-2.openshiftapps.com/techu-bpjjsa/v1/hello')
        .get('/')
        .end(
          function(err, res) {
            // console.log(res);
            console.log("Request has finished");
            console.log(err);
            res.should.have.status(200);
            done();
          }
        )
      }
    )
  }
)

describe ('Test de API',
  function() {
    it('Prueba que la API responde correctamente', function(done) {
      //   chai.request('http://localhost:3000/')
        chai.request('https://techu-techu-bpjjsa.7e14.starter-us-west-2.openshiftapps.com/')
        .get('techu-bpjjsa/v1/hello')
        .end(
          function(err, res) {
            console.log("Request has finished");
            // console.log(err);
            res.should.have.status(200);
            res.body.msg.should.be.eql("Hola desde API TechU");
            done();
          }
        )
      }
    ),
    it('Prueba que la API devuelve una lista de usuarios correctos', function(done) {
      //   chai.request('http://localhost:3000/')
        chai.request('https://techu-techu-bpjjsa.7e14.starter-us-west-2.openshiftapps.com/')
        .get('techu-bpjjsa/v1/users')
        .end(
          function(err, res) {
            console.log("Request has finished");
            // console.log(err);
            res.should.have.status(200);
            res.body.users.should.be.a('array');

            for (user of res.body.users) {
              user.should.have.property('email');
            //   user.should.have.property('first_name');
            }
            done();
          }
        )
      }
    )
  }
);
